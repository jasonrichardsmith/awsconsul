package consulapi

import (
	"encoding/json"
	"fmt"

	consul "github.com/hashicorp/consul/api"
)

type API struct {
	*consul.Config
	*consul.Client
}

type Services []Service
type Service consul.AgentServiceRegistration

func New() API {
	config := API{
		Config: consul.DefaultConfig(),
	}
	return config
}

func NewServices() Services {
	s := make([]Service, 0)
	return s
}

func (a *API) SetAddress(serveraddress, port string) {
	a.Config.Address = fmt.Sprintf("%s:%s", serveraddress, port)
}

func (a *API) getClient() (err error) {
	a.Client, err = consul.NewClient(a.Config)
	return err
}

func (a *API) RegisterServices(services Services) (errs []error) {
	errs = make([]error, 0)
	for _, service := range services {
		err := a.RegisterService(service)
		if err != nil {
			errs = append(errs, err)
		}
	}
	return errs
}

func (a *API) RegisterService(service Service) error {
	err := a.getClient()
	if err != nil {
		return err
	}
	agent := a.Agent()
	cs := consul.AgentServiceRegistration(service)
	return agent.ServiceRegister(&cs)
}

func ServiceFromJson(jsonbytes []byte) (Service, error) {
	service := Service{}
	err := json.Unmarshal(jsonbytes, &service)
	return service, err
}

func ServicesFromMultipleJson(jsonbytes [][]byte) (Services, []error) {
	services := NewServices()
	errs := make([]error, 0)
	for _, b := range jsonbytes {
		s, err := ServiceFromJson(b)
		if err != nil {
			errs = append(errs, err)
		} else {
			services = append(services, s)
		}
	}
	return services, errs
}

func ServicesFromSingleJson(jsonbytes []byte) (Services, error) {
	services := NewServices()
	err := json.Unmarshal(jsonbytes, &services)
	return services, err
}
