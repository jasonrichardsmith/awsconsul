package main

import (
	"time"

	"codeberg.org/jasonrichardsmith/awsconsul/awsapi"
	"codeberg.org/jasonrichardsmith/awsconsul/consulapi"
	"codeberg.org/jasonrichardsmith/awsconsul/reconciler"
)

func main() {
	aws := awsapi.New("eu-west-1")
	consul := consulapi.New()
	reconcile := reconciler.New(aws, consul, 10)
	reconcile.Start()

}

type Config struct {
	ConsulPort string        `json:""`
	Region     string        `json:""`
	Interval   time.Duration `json:""`
}
