package reconciler

import (
	"errors"
	"fmt"
	"strings"
	"time"

	"codeberg.org/jasonrichardsmith/awsconsul/awsapi"
	"codeberg.org/jasonrichardsmith/awsconsul/consulapi"
)

type Reconciler struct {
	aws      awsapi.API
	consul   consulapi.API
	interval time.Duration
}

func New(awsapi awsapi.API, consulapi consulapi.API, interval time.Duration) Reconciler {
	return Reconciler{awsapi, consulapi, interval}
}

func (r *Reconciler) Start() {
	for {
		time.Sleep(r.interval * time.Second)
		errs := r.reconcileInstances()
		for _, err := range errs {
			fmt.Println(err)
		}
		errs = r.reconcileAliases()
		for _, err := range errs {
			fmt.Println(err)
		}

	}

}

func (r *Reconciler) reconcileInstances() []error {
	var errs []error
	insts, err := r.aws.GetInstances("consulserver")
	if err != nil {
		return append(errs, fmt.Errorf("[ERROR] Consul Server Discovery: %s\n", err))
	}
	if len(insts) == 0 {
		return append(errs, errors.New("[ERROR] No Consul Servers found"))
	}
	r.consul.SetAddress(insts[0].IP, "8500")
	insts, err = r.aws.GetInstances("service")
	if err != nil {
		return append(errs, fmt.Errorf("[ERROR] Service Discovery: %s\n", err))
	}
	services := consulapi.NewServices()
	for _, inst := range insts {
		doc, err := r.aws.GetConfiguration(inst.ConfigPath)
		if err != nil {
			errs = append(errs, fmt.Errorf("[ERROR] Service Configuration: IP %s %s\n", inst.IP, err))
			continue
		}
		doc = replaceHealthRoute(doc, inst.IP)
		instservices, err := consulapi.ServicesFromSingleJson(doc)
		if err != nil {
			errs = append(errs, fmt.Errorf("[ERROR] Service Configuration: ID %s %s\n", inst.ID, err))
			continue
		}
		for i, service := range instservices {
			service.ID = fmt.Sprintf("%s-%v", inst.ID, i)
			service.Address = inst.IP
			services = append(services, service)
		}

	}
	consulerrs := r.consul.RegisterServices(services)
	return append(errs, consulerrs...)
}

func (r *Reconciler) reconcileAliases() []error {
	jsonbytes, errs := r.aws.GetPathConfigurations("/consulserveraliases/")
	services, marshalerrs := consulapi.ServicesFromMultipleJson(jsonbytes)
	errs = append(errs, marshalerrs...)
	consulerrs := r.consul.RegisterServices(services)
	return append(errs, consulerrs...)
}

func replaceHealthRoute(doc []byte, ip string) []byte {
	str := strings.Replace(string(doc), "HEALTHROUTE", ip, -1)
	return []byte(str)
}
