package awsapi

import (
	"encoding/base64"
	"fmt"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ec2"
	"github.com/aws/aws-sdk-go/service/ssm"
)

type API struct {
	aws.Config
}

type Instance struct {
	IP         string
	ID         string
	ConfigPath string
}

func New(region string) API {
	return API{
		Config: aws.Config{Region: &region},
	}
}

func (a *API) GetInstances(cloudtag string) ([]Instance, error) {
	var instances []Instance
	svc := ec2.New(session.New(), &a.Config)
	resp, err := svc.DescribeInstances(&ec2.DescribeInstancesInput{
		Filters: []*ec2.Filter{
			&ec2.Filter{
				Name:   aws.String("tag:consulcloud"),
				Values: []*string{aws.String(cloudtag)},
			},
			&ec2.Filter{
				Name:   aws.String("instance-state-name"),
				Values: []*string{aws.String("running")},
			},
		},
	})

	if err != nil {
		return instances, fmt.Errorf("[ERROR] EC2 Discovery: DescribeInstances failed for tag %s: %s", cloudtag, err)
	}
	for _, r := range resp.Reservations {
		for _, inst := range r.Instances {
			if inst.PrivateIpAddress == nil {
				fmt.Sprintf("[ERROR] EC2 Discovery: Instance %s for tag %s has no private ip\n",
					*inst.InstanceId, cloudtag)
				continue
			} else {
				i := Instance{
					IP: *inst.PrivateIpAddress,
					ID: *inst.InstanceId,
				}
				for _, tag := range inst.Tags {
					if *tag.Key == "consulserviceconfig" {
						i.ConfigPath = *tag.Value
					}
				}
				instances = append(instances, i)
			}
		}
	}
	return instances, nil
}

func (a *API) GetConfiguration(name string) ([]byte, error) {
	svc := ssm.New(session.New(), &a.Config)
	resp, err := svc.GetParameter(&ssm.GetParameterInput{Name: &name})
	if err != nil {
		return make([]byte, 0), err
	}
	return base64.StdEncoding.DecodeString(*resp.Parameter.Value)

}

func (a *API) GetPathConfigurations(path string) ([][]byte, []error) {
	errs := make([]error, 0)
	docs := make([][]byte, 0)
	svc := ssm.New(session.New(), &a.Config)
	resp, err := svc.GetParametersByPath(&ssm.GetParametersByPathInput{Path: &path})
	if err != nil {
		return docs, append(errs, err)
	}
	for _, param := range resp.Parameters {
		doc, err := base64.StdEncoding.DecodeString(*param.Value)
		if err != nil {
			errs = append(errs, err)
		} else {
			docs = append(docs, doc)
		}
	}

	return docs, errs
}
